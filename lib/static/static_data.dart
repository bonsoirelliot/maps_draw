import 'package:flutter/material.dart';

class StaticData {
  static const defaultPadding = 12.0;
}

class Keys {
  static final scaffoldKey = GlobalKey<ScaffoldState>();
}
