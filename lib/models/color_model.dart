import 'package:flutter/cupertino.dart';

class ColorModel {
  final String name;
  final Color color;

  ColorModel({
    required this.name,
    required this.color,
  });
}
